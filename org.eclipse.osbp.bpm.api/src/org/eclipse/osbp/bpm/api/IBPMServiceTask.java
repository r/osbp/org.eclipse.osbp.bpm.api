/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */

package org.eclipse.osbp.bpm.api;

public interface IBPMServiceTask extends IBlipBPMUserTask {
	ServiceExecutionMode getExecutionMode();
	int getTimeoutInSecs();
	ServiceImplementation getImplementation();
	String getFunctionLibraryClass();
	String getFunctionLibraryMethod();
	String getWebServiceInterface();
	String getWebServiceOperation();
	String getJavaInterface();
	String getJavaOperation();
	String getParameterType();
}

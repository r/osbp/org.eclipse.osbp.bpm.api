/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.bpm.api;

import java.util.List;
import java.util.Map;

import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.ui.api.user.IUser;

public interface IBPMTaskClient extends IBlipBPMConstants {

	boolean operations(final long taskId, final IUser user, final IUser alternateUser, final BPMOperation operation);
   	BPMTaskSummary getUserTask(final Long taskId, final IUser user);
	List<BPMTaskSummary> getUserTaskList(final IUser user, boolean owned);
	void subscribeProcessCompletedEventNotification(IBPMTaskEventNotification notification);
	void unsubscribeProcessCompletedEventNotification(IBPMTaskEventNotification notification);
	void subscribeTaskEventNotification(IBPMTaskEventNotification notification);
	void subscribeTaskEventNotification(BPMTaskEventType type, IBPMTaskEventNotification notification);
	void unsubscribeTaskEventNotification(IBPMTaskEventNotification notification);
	void unsubscribeTaskEventNotification(BPMTaskEventType type, IBPMTaskEventNotification notification);
	IDto getWorkloadDto(Object taskInformationObject);
	String getWorkloadDtoFqn(Object taskInformationObject);
	void setWorkloadDtoFqn(Object taskInformationObject, String workloadDtoFqn);
	void setWorkloadDto(Object taskInformationObject, IDto workloadDto);
	String getProcessId(BPMTaskSummary taskSummary);
	void registerPerspectiveListener(BPMTaskClientPerspectiveListener listener);
	void unregisterPerspectiveListener(BPMTaskClientPerspectiveListener listener);
}

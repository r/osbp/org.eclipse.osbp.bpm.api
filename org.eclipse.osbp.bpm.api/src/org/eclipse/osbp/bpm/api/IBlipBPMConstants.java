/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.bpm.api;

public interface IBlipBPMConstants {

	/** variable name for the instance of {@link org.eclipse.osbp.bpm.api.IBlipBPMProcessProtocol} with protocol about this process */
	public final static String VARIABLE_PROCESS_PROTOCOL = "processProtocol";
	/** variable name for the instance of {@link org.eclipse.osbp.bpm.api.IBlipBPMStartInfo} or null */
	public final static String VARIABLE_PROCESS_START_INFO = "processStartInfo";
	/** variable name for the full qualified class name as String of the workload dto or null */
	public final static String VARIABLE_PROCESS_WORKLOAD_DTO_FQN = "processWorkloadDtoFQN";
	/** variable name for the instance of the workload dto of {@link org.eclipse.osbp.dsl.common.datatypes.IDto} or null */
	public final static String VARIABLE_PROCESS_WORKLOAD_DTO = "processWorkloadDto";

	/** variable name for the instance of {@link org.jbpm.task.query.TaskSummary} or null */
	public final static String VARIABLE_TASK_JBPM_SUMMARY = "taskSummary";
	/** variable name for the instance of {@link org.eclipse.osbp.bpm.api.IBlipBPMWorkloadModifiableItem} or null */
	public final static String VARIABLE_TASK_START_INFO = "taskStartInfo";
	/** variable name for the instance of {@link org.eclipse.osbp.xtext.blip.BlipItem} or null */
	public final static String VARIABLE_TASK_BLIP_ITEM = "taskBlipItem";
	/** variable name for the instance of {@link org.eclipse.bpmn2.Task} or null */
	public final static String VARIABLE_TASK_BPM2_ITEM = "taskBpmn2Task";
	/** variable name for the operative dto class of {@link Class<?>} or null */
	public final static String VARIABLE_TASK_OPERATIVE_DTO_CLASS = "operativeDtoClass";
	/** variable name for the full qualified class name as String of operative dto or null */
	public final static String VARIABLE_TASK_OPERATIVE_DTO_FQN = "operativeDtoFQN";
	/** variable name for the instance of the list of operative dtos of {@link java.util.List<org.eclipse.osbp.dsl.common.datatypes.IDto>} or null */
	public final static String VARIABLE_TASK_OPERATIVE_DTOS = "operativeDtos";
}

/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.bpm.api;

/**
 *  Interface for a class, which caches all protocols for process instances during run time.
 */
public interface IBlipBPMProcessProtocolCache {
	/**
	 *  @param  context     should be inspected to contain the process instance id
	 *  @return             the protocol instance or create a new empty instance
	 */
	IBlipBPMProcessProtocol getProtocol(Object context);
	/**
	 *  @param  context     should be inspected to contain the process instance id
	 *  @param  line        information to be added
	 *  @return             the protocol instance
	 */
	IBlipBPMProcessProtocol addToProtocol(Object context, String line);
	/**
	 *  @param  context     should be inspected to contain the process instance id
	 *  @param  line        information to be added
	 *  @param  toBeMerged  another protocol instance, which has to be merged into this protocol instance
	 *  @return             the protocol instance
	 */
	IBlipBPMProcessProtocol addToProtocol(Object context, String line, IBlipBPMProcessProtocol toBeMerged);
	/**
	 *  @param  context     should be inspected to contain the process instance id
	 *  @param  toBeMerged  another protocol instance, which has to be merged into this protocol instance
	 *  @return             the protocol instance
	 */
	IBlipBPMProcessProtocol mergeToProtocol(Object context, IBlipBPMProcessProtocol toBeMerged);
	/**
	 *  @param  context     should be inspected to contain the process instance id, for which the protocol can be removed from the cache
	 */
	void removeFromCache(Object context);
}

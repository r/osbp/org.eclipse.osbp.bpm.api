/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.bpm.api;

public enum ServiceImplementation {
	FUNCTION_LIBRARY("##FunctionLibrary"),
	WEB_SERVICE("##WebService"),	// see org.jbpm.process.workitem.bpmn2.ServiceTaskHandler.executeWorkItem(WorkItem,WorkItemManager)
	JAVA_METHOD("##default");
	
	private final String implementation;
	
	private ServiceImplementation(String implementation) {
		this.implementation = implementation;
	}
	
	public String getImplementation() {
		return implementation;
	}
}